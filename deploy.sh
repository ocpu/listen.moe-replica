#!/bin/bash

ssh node@178.62.25.11 << 'ENDSSH'
cd ~/apps/listen.moe-replica/
git pull
rm -rf node_modules
npm i
ENDSSH