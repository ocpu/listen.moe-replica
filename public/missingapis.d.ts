type artwork = { src: string, sizes: string, type: string }

declare class MediaMetadata {
    constructor(options: { title?: string, artist?: string, album?: string, artwork?: artwork[] })
    title?: string
    artist?: string
    album?: string
    artwork?: artwork[]
}

interface ActionHandlerEventMap {
    'play': null
    'pause': null
    'seekbackward': null
    'seekforward': null
    'previoustrack': null
    'nexttrack': null
}

interface WebMediaSession {
    playpackState: 'none' | 'paused' | 'playing'
    metadata?: MediaMetadata
    setActionHandler<E extends keyof ActionHandlerEventMap>(type: E, handler: () => void)
    setActionHandler(type: string, handler: () => void)
}

interface BatteryChargingEvent extends Event {
    charging: boolean
}
interface BatteryChargingTimeEvent extends Event {
    chargingTime: number
}
interface BatteryDischargingEvent extends Event {
    dischargingTime: number
}
interface BatteryLevelEvent extends Event {
    level: number
}

interface BatteryManagerEventMap {
    'chargingchange': BatteryChargingEvent
    'chargingtimechange': BatteryChargingTimeEvent
    'dischargingchange': BatteryDischargingEvent
    'levelchange': BatteryLevelEvent
}

interface BatteryManager extends EventTarget {
    readonly charging: boolean
    readonly chargingTime: number
    readonly dischargingTime: number
    readonly level: number
    onchargingchange: (this: BatteryManager, ev: BatteryChargingEvent) => void
    onchargingtimechange: (this: BatteryManager, ev: BatteryChargingTimeEvent) => void
    ondischargingchange: (this: BatteryManager, ev: BatteryDischargingEvent) => void
    onlevelchange: (this: BatteryManager, ev: BatteryLevelEvent) => void
    addEventListener<K extends keyof BatteryManagerEventMap>(type: K, listener: (this: BatteryManager, ev: BatteryManagerEventMap[K]) => any, useCapture?: boolean)
    addEventListener(type: string, listener: EventListenerOrEventListenerObject, useCapture?: boolean)
}

interface NetworkInfornationEventMap {
    'change': Event
}

interface NetworkInfornation extends EventTarget {
    readonly type: 'bluetooth' | 'cellular' | 'ethernet' | 'none' | 'wifi' | 'wimax' | 'other' | 'unknown'
    readonly downlinkMax?: number
    onchange: (this: NetworkInfornation, ev: Event) => any
    addEventListener<K extends keyof NetworkInfornationEventMap>(type: K, listener: (this: NetworkInfornation, ev: K) => any, useCapture: boolean)
    addEventListener(type: string, listener: EventListenerOrEventListenerObject, useCapture: boolean)
}/*

interface VRDisplayCapabilities {
    readonly canPresent: boolean
    readonly hasExternalDisplay: boolean
    readonly hasOrientation: boolean
    readonly hasPosition: boolean
    readonly maxLayers: number
}

interface VRStageParameters {
    readonly sizeX
    readonly sizeY
    readonly sittingToStandingTransform: Float32Array
}

interface VREyeParameters {
    readonly offest: number
    readonly fieldOfView: number
    readonly renderWidth: number
    readonly renderHeight: number
}

interface VRDisplay {
    readonly capabilities: VRDisplayCapabilities
    depthFar: number
    depthNear: number
    readonly displayId: number
    readonly displayName: string
    readonly isConnected: boolean
    readonly isPresenting: boolean
    readonly stageParameters: VRStageParameters
    getEyeParameters(): VREyeParameters
}*/

interface Navigator {
    readonly connection?: NetworkInfornation
    mediaSession?: WebMediaSession
    getBattery(): Promise<BatteryManager>
}