const VERSION = 0
const CACHE_NAME = 'Listen.moe-cache-1'
const ASSETS = [
    '/',
    '/index.js',
    '/main.css',
    '/images/background.webp',
    '/images/favicon-16x16.png',
    '/images/favicon-32x32.png',
    '/images/favicon-192x192.png',
    '/images/favicon-512x512.png',
    '/images/kanna.mp4',
    '/images/tomori.png',
    '/images/play.svg',
    '/images/pause.svg',
    '/images/volume-down.svg',
    '/images/volume-mute.svg',
    '/images/volume-off.svg',
    '/images/volume-up.svg',
]

self.oninstall = event => event.waitUntil((async () => {
  const cache = await caches.open(CACHE_NAME)
  await cache.addAll(ASSETS)
  return self.skipWaiting()
})())

self.onactivate = event => event.waitUntil(self.clients.claim())

self.onfetch = event => event.respondWith((async () => {
    const response = await caches.match(event.request)
    if (response)
        return response
    return fetch(event.request.clone()).then(async res => {
        if (!res || res.status !== 200 || res.type !== 'basic')
            return res
        const cache = await caches.open(CACHE_NAME)
        cache.put(event.request, res.clone())
        return res
    })
})())
