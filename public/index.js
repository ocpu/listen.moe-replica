function createJSONSocket(url) {
    const ws = new WebSocket(url)

    const listeners = {
        'open': [],
        'message': [],
        'close': [],
        'error': [],
    }

    function genericEmit(event) {
        for (let listener of listeners[event.type])
            listener(event)
    }

    ws.addEventListener('open', genericEmit)
    ws.addEventListener('error', genericEmit)
    ws.addEventListener('close', genericEmit)
    ws.addEventListener('message', ({ data }) => {
        if (!data) data = '{}'
        const json = JSON.parse(data)
        for (let listener of listeners.message)
            listener(json)
    })

    return {
        on(type, listener) {
            if (type in listeners && !listeners[type].includes(listener))
                listeners[type].push(listener)
        },
        off(type, listener) {
            if (type in listeners && listeners[type].includes(listener))
                return listeners[type].splice(listeners[type].indexOf(listener), 1)[0]
        },
        /*once(type, filter, listener) {
            if (!(type in listeners))
                return
            this.on(type, json => {

            })
        },*/
        send(json) {
            ws.send(JSON.stringify(json))
        },
        sendRaw(data) {
            ws.send(data)
        }
    }
}
function createListenSocket(messageListener) {
    let socket = createJSONSocket('wss://listen.moe/api/v2/socket')
    let authPromiseResolve
    let user_token
    let isAuthenticated = false
    socket.on('message', data => {
        if (authPromiseResolve && 'extended' in data) {
            authPromiseResolve()
            isAuthenticated = true
            authPromiseResolve = void 0
        }
        if ('reason' in data) {
            console.log('Connection is cloasing. Reason:', data.reason)
            return
        }
        if (!('song_id' in data))
            return
        const info = {
            id: data.song_id,
            title: data.song_name,
            artist: data.artist_name,
            anime: data.anime_name,
            history: [
                { title: data.last.song_name, artist: data.last.artist_name },
                { title: data.second_last.song_name, artist: data.second_last.artist_name }
            ],
            listeners: data.listeners,
            requestedBy: data.requested_by
        }
        if ('extended' in data) {
            info.favorite = data.extended.favorite
            info.queue = {
                songAmount: data.extended.queue.songsInQueue,
                hasSong: data.extended.queue.hasSongsInQueue
            }
            info.user = {
                position: data.extended.queue.inQueueBeforeUserSong,
                songsInQueue: data.extended.queue.userSongsInQueue,
            }
        }
        messageListener(info)
    })

    let ret = {
        async authenticate(token) {
            user_token = token
            const promise = new promise(r => void (authPromiseResolve = r))
            socket.send({ token })
            await promise
        },
        update() {
            socket.sendRaw('update')
        }
    }
    socket.on('open', () => {
        console.log('Connection established')
    })
    socket.on('close', () => {
        console.log('Socket connection closed retrying in 5sec')
        setTimeout(() => {
            ret = createListenSocket(messageListener)
            if (isAuthenticated)
                ret.authenticate(user_token)
        }, 5000)
    })
    return ret
}

const socket = createListenSocket(data => {
    console.log(data)
    document.querySelector('.listeners').innerHTML = `${data.listeners} ${data.listeners === 1 ? 'user' : 'users'} listening`
    if ('mediaSession' in navigator) navigator.mediaSession.metadata = new MediaMetadata({
        title: data.title,
        artist: data.artist,
        album: data.anime
    })
})



const audioEl = document.querySelector('audio')
audioEl.removeAttribute('controls')
//audioURL = URL.createObjectURL('')
const controlsEl = document.querySelector('.controls')
controlsEl.removeAttribute('hidden')
const audioStateImage = document.querySelector('.play-pause > img')
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js')
}
if ('mediaSession' in navigator) {
    navigator.mediaSession.setActionHandler('play', () => {
        audioEl.play()
        navigator.mediaSession.playpackState = 'playing'
    })
    navigator.mediaSession.setActionHandler('pause', () => {
        audioEl.pause()
        navigator.mediaSession.playpackState = 'paused'
    })
    
}

controlsEl.querySelector('.play-pause').addEventListener('click', event => {
    const self = event.target
    if (audioEl.paused) {
        audioEl.play()
        audioStateImage.src = '/images/pause.svg'
        requestAnimationFrame(frameLooper)
    } else {
        audioEl.pause()
        audioStateImage.src = '/images/play.svg'
    }
})

// Because of cors
const canvas = document.querySelector('canvas')
const ctx = canvas.getContext('2d')
const context = new AudioContext()
const analyser = context.createAnalyser()
const source = context.createMediaElementSource(audioEl)
source.connect(analyser)
analyser.connect(context.destination)

const frameLooper = () => {
    if (audioEl.paused)
        return
    requestAnimationFrame(frameLooper)
    const fbc_array = new Uint8Array(analyser.frequencyBinCount)
    analyser.getByteFrequencyData(fbc_array)
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    ctx.fillStyle = '#ec1a55'
    for (let i = 0; i < 100; i++) {
        ctx.fillRect(i * 2, canvas.height, 2, -(fbc_array[i] / 2))
    }
}

const ws = new WebSocket('wss://listen.moe/api/v2/socket')

let songData

ws.addEventListener('open', console.log)
ws.addEventListener('message', ({ data }) => {
    if (!data) return
    const old = songData
    songData = JSON.parse(data)
    if ('reason' in songData) {
        songData = old
        return
    }
    console.log(songData)
    document.querySelector('.listeners').innerHTML = `${songData.listeners} ${songData.listeners === 1 ? 'user' : 'users'} listening`
    if ('mediaSession' in navigator) navigator.mediaSession.metadata = new MediaMetadata({
        title: songData.song_name,
        artist: songData.artist_name,
        album: songData.anime_name
    })
})
/*
fetch('https://listen.moe/stream', {
    method: 'GET',
    mode: 'no-cors'
}).then(res => res.blob(), console.error).then(console.log)*/
/*
const xhr = new XMLHttpRequest
xhr.open('GET', 'https://listen.moe/stream', true);
xhr.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
xhr.responseType = 'blob';
xhr.onload = function(evt) {
    var blob = new Blob([xhr.response], {type: 'audio/ogg'});
    var objectUrl = URL.createObjectUrl(blob);
    let audio = new Audio()
    audio.src = objectUrl;
    // Release resource when it's loaded
    audio.onload = function(evt) {
    URL.revokeObjectUrl(objectUrl);
    };
    audio.play();
};
xhr.send();*/