const express = require('express')
const app = express()
const path = require('path')

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))

const redirectStream = (url, type) => (req, res) => {
    require('https').get(url, streamRes => {
        res.setHeader('Content-Type', type)
        streamRes.on('data', data => res.write(data, type))
    })
}

app.use((req,_,next) => {
    console.log(`${req.method} ${req.path}`)
    next()
})
app.get('/stream', redirectStream('https://listen.moe/stream', 'audio/ogg'))
app.get('/fallback', redirectStream('https://listen.moe/fallback', 'audio/aac'))
app.get('/', (req, res) => res.render('index'))
app.use(express.static(require('path').join(__dirname, 'public')))

const readFile = require('util').promisify(require('fs').readFile)
Promise.all([
    readFile('/etc/letsencrypt/live/opencubes.io/fullchain.pem', 'utf-8'),
    readFile('/etc/letsencrypt/live/opencubes.io/privkey.pem', 'utf-8')
]).then(
    ([cert, key]) => require('https').createServer({ cert, key }, app),
    () => require('http').createServer(app)
).then(server => {
    server.listen(8080).addListener('listening', () => console.log('listening on port 8080'))
})
